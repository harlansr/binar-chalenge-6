class Home {
  // View
  static sendView(req, res, next) {
    const payload = {
      name: "kevin",
      address: "earth",
      friends: ["aldo", "aksa", "jr"]
    }
    res.render("index", payload)
  }

  static home(req, res, next) {
    const data = {
      login: req.session.login,
      name: req.session.name,
    }
    res.render("home", data)
  }
  static game(req, res, next) {
    const data = {
      name: req.session.name,
      user_id: req.session.user_id,
    }
    res.render("game", data)
  }

}

module.exports = Home