
class Middleware {
  static putMiddleware(req, res, next) {
    console.log("=== Middleware  ===")
    console.log(req.body)
    const name = req.body.name
    next()
    // if (name == '') {
    //   // res.send("Harus Kirim Nama")
    // } else {
    //   next()
    // }
  }

  static errorHandler(err, req, res, next) {
    console.log("=== error handler ===")
    if (err.status) {
      const _status = err.status
      res.status(_status).json(err)
    }
    else {
      res.status(400).json({
        messages: "Ooppss",
        error: err
      })
    }
  }

  static doWithAuth(req, res, next) {
    if (req.session.login) {
      next()
    } else {
      res.redirect('/login');
    }
  }

  static doWithoutAuth(req, res, next) {
    if (req.session.login) {
      res.redirect('/');
    } else {
      next()
    }
  }


}

module.exports = Middleware